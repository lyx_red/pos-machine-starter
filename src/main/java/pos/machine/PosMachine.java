package pos.machine;

import java.util.*;

public class PosMachine {
    public String printReceipt(List<String> barcodes) {
        List<ReceiptItem> receiptItems = decodeToItems(barcodes);
        Receipt receipt = calculateCost(receiptItems);
        return renderReceipt(receipt);
    }

    public List<Item> loadAllItems() {
        List<Item> items = ItemsLoader.loadAllItems();
        return items;
    }

    public List<ReceiptItem> decodeToItems(List<String> barcodes) {
        List<Item> items = loadAllItems();
        HashMap<String, Integer> map = new HashMap<>();

        List<ReceiptItem> receiptItems = new ArrayList<>();
        for (String barcode : barcodes) {
            map.put(barcode, map.getOrDefault(barcode, 0)+1);
        }

        items.stream().forEach(item -> {
            String barcode = item.getBarcode();
            if(map.containsKey(barcode)) {
                receiptItems.add(new ReceiptItem(item.getName(),map.get(barcode),item.getPrice()));
            }
        });

        return receiptItems;
    }

    public Receipt calculateCost(List<ReceiptItem> receiptItems) {
        return new Receipt(calculateItemsCost(receiptItems), calculateTotalPrice(receiptItems));
    }



    private List<ReceiptItem> calculateItemsCost(List<ReceiptItem> receiptItems) {
        for (ReceiptItem receiptItem : receiptItems) {
            receiptItem.setSubTotal(receiptItem.getQuantity() * receiptItem.getUnitPrice());
        }
        return receiptItems;
    }

    public int calculateTotalPrice(List<ReceiptItem> receiptItems) {
        int totalPrice = 0;
        for (ReceiptItem receiptItem : receiptItems) {
            totalPrice += receiptItem.getSubTotal();
        }
        return totalPrice;
    }

    public String renderReceipt(Receipt receipt) {
        String itemsReceipt = generateItemsReceipt(receipt);
        return generateReceipt(itemsReceipt, receipt.getTotalPrice());
    }



    public String generateReceipt(String itemsReceipt, int totalPrice) {
        String receipt = "***<store earning no money>Receipt***\n" +
                itemsReceipt +
                "----------------------\n" +
                "Total: "+ totalPrice +" (yuan)\n" +
                "**********************";
        return receipt;
    }

    public String generateItemsReceipt(Receipt receipt) {
        List<ReceiptItem> receiptItems = receipt.getReceiptItems();
        StringBuilder itemsReceipt = new StringBuilder();
        for (ReceiptItem receiptItem : receiptItems) {
            StringJoiner itemReceipt = new StringJoiner(", ");
            itemReceipt.add("Name: " + receiptItem.getName());
            itemReceipt.add("Quantity: " + receiptItem.getQuantity());
            itemReceipt.add("Unit price: " + receiptItem.getUnitPrice() + " (yuan)");
            itemReceipt.add("Subtotal: " + receiptItem.getSubTotal() + " (yuan)");
            itemsReceipt.append(itemReceipt);
            itemsReceipt.append("\n");
        }
        return itemsReceipt.toString();
    }


}
