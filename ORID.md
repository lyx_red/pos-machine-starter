# ORID



## Objective

- This morning, we mainly studied tasking, Context Map and Naming. At the same time, I have learned some basic knowledge of git, and have a deeper impression on git's common commands.
- In the afternoon, I mainly did some practical exercises about Context Map and tasking. At the same time, the first small program was implemented according to the Context Map drawn, which made me consolidate the new knowledge.



## Reflective

I feel fulfilled and excited

## Interpretative

Because today learned a lot of knowledge, such as tasking, git, Context Map and so on. It makes me feel fulfilled, and it makes me excited to be exposed to new knowledge.

## Decision

Before learning tasking, we usually carried out the development when we got the demand. However, after learning the concept of tasking, I understood that we should analyze the demand and decompose the task before the development, and then draw the Context Map according to the decomposition. The Context Map allows us to be more clear about the development process. I also pay more attention to the naming of functions or variables in the development process. So I will apply the new knowledge I learned today
